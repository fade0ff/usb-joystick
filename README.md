A flexible USB HID-Joystick implementation

See http://lemmini.de/USB-Joystick/USB-Joystick.html

---------------------------------------------------------------
Copyright 2019 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.