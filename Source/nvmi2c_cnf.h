/** NVM library - Configuration
	Configuration of non volatile memory - I2C EEPROM (AT24C256/AT24C128)
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef NVMI2C_CNF_H
#define NVMI2C_CNF_H

#define NVM_I2C_SIZE                32768     ///! number of bytes in NVRAM

/* I2C specific */

#define NVM_I2C_PAGE_SIZE           64        ///! number of bytes per page for page writes

/* Project specific */


#endif
