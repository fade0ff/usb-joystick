/** SSD1306 OLED Library
	Communication through I2C mode
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2019 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "i2c.h"
#include "systime.h"
#include "fonts.h"
#include "ssd1306.h"
#include "ssd1306_cnf.h"
#include <string.h>


u8 ssd1306_screenbuffer[SSD1306_SIZE_Y*SSD1306_SIZE_X/8]; // monochrome: one byte represents 8 (vertical) pixels

#define SSD1306_CTRL_DATA 0x40
#define SSD1306_CTRL_CMD  0x00

static u8 tx_data[10];

/** Lookup table for 8bit commands.
 *  Note: this might seem futile on 1st look, but this const table can be used translate a command to a fixed address containing this command.
 *  This makes it possible to enqueue commands with only a pointer to the TX buffer known.
 */
const u8 ssd1306_cmd[256] = {
	  0,   1,  2,    3 ,  4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,
	 32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
	 64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,
	 96,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
	128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
	160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
	192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
	224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
};

/**
 * Write command without parameters
 * @param cmd command
 */
void SSD1306_WriteCommand(u8 cmd) {
	while (I2C_GetChannelPendingCount(SSD1306_I2C_CH));
	(void)I2C_MasterTransfer(SSD1306_I2C_CH, I2C_TYPE_MST_WRITE, SSD1306_CTRL_CMD, (u8*)&ssd1306_cmd[cmd], 1, NULL);
}

/**
 * Write number of commands and parameters
 * @param commands pointer to commands and parameters
 * @param len number of bytes
 */
void SSD1306_WriteCommands(u8 *commands, u16 len) {
	while (I2C_GetChannelPendingCount(SSD1306_I2C_CH));
	(void)I2C_MasterTransfer(SSD1306_I2C_CH, I2C_TYPE_MST_WRITE, SSD1306_CTRL_CMD, commands, len, NULL);
}

/**
 * Write number of data bytes
 * @param data pointer to data
 * @param len number of bytes
 */
void SSD1306_WriteData(u8 *data, u16 len) {
	while (I2C_GetChannelPendingCount(SSD1306_I2C_CH));
	(void)I2C_MasterTransfer(SSD1306_I2C_CH, I2C_TYPE_MST_WRITE, SSD1306_CTRL_DATA, data, len, NULL);
}

/**
 * Set page/column pointers
 * @param c 1st column to access  [0..127]
 * @param p 1st page to access    [0..7]
 */
void SSD1306_SetStart(u8 c, u8 p) {
	tx_data[0] = SSD1306_LOWER_COLUMN(c&0xf);
	tx_data[1] = SSD1306_HIGHER_COLUMN(p&0xf0);
	tx_data[2] = SSD1306_PAGE_START(p);
}

/**
 * Set area where the controller can access the graphics buffer
 * @param c1 1st column to access  [0..127]
 * @param p1 1st page to access    [0..7]
 * @param c1 last column to access [0..127]
 * @param p2 last page to access   [0..7]
 */
void SSD1306_SetArea(u8 c1, u8 p1, u8 c2, u8 p2) {
	tx_data[0] = SSD1306_COLUMN_AREA;
	tx_data[1] = c1 & 0x7f;
	tx_data[2] = c2 & 0x7f;
	tx_data[3] = SSD1306_PAGE_AREA;
	tx_data[4] = p1 & 0x7;
	tx_data[5] = p2 & 0x7;
	SSD1306_WriteCommands(tx_data, 6);
}

/**
 * Set area where the controller can access the graphics buffer to full screen
 */
void SSD1306_SetFullScreen(void) {
	SSD1306_SetArea(0,0,SSD1306_SIZE_X-1,7);
}

/**
 * Draw a single pixel in the screen buffer
 * Note:
 * 128x64bits in 8 Pages
 * Each page consists of 128 segments (bytes).
 * Each byte is interpreted vertically (D0 is top, D7 id bottom).
 * -> One page equals 8 rows (vertical byte vector) and 128 columns (128 bytes)
 * x is the column/segment: 0..127 and can be used directly as column offset
 * y selects the page (y/8) and the vertical position within the page ( 1<<(y&7) ).
 */
void SSD1306_DrawPixel(u8 x, u8 y, u8 set) {
	u8 *ptr = &ssd1306_screenbuffer[(y/8)*SSD1306_SIZE_X+x];
	u8 mask = 1<<(y&7);
	if (set)
		*ptr |= mask;
	else
		*ptr &= (u8)~mask;
}

/**
 * Fill a rectangle with the given color
 * @param c1 x/column coordinate of upper left corner
 * @param p1 page of upper left corner
 * @param c2 x coordinate of lower right corner
 * @param p1 page coordinate of lower right corner
 * @param set 0: clear, 1: set
 */
void SSD1306_FillRect(u8 c1, u8 p1, u8 c2, u8 p2, u8 set) {
	u8 c,p;
	u8 *ptr;
	u8 b = (set==0) ? 0 : 0xff;

	for (p=p1; p <= p2; p++) {
		ptr = &ssd1306_screenbuffer[p*SSD1306_SIZE_X+c1];
		for (c=c1; c <= c2; c++) {
			*ptr = b;
			ptr++;
		}
	}
}

/**
 * Fill screen with given color
 * @param set 0: clear, 1: set
 */
void SSD1306_FillScreen(u8 set) {
	u16 i;
	u8 b = (set==0) ? 0 : 0xff;
	for (i=0; i < sizeof(ssd1306_screenbuffer); i++) {
		ssd1306_screenbuffer[i] = b;
	}
}

/**
 * Fill a rectangle with the given data
 * @param c1 x/column coordinate of upper left corner
 * @param p1 page of upper left corner
 * @param c2 x coordinate of lower right corner
 * @param p1 page coordinate of lower right corner
 * @param data data buffer containing 16bit colors
 */
void SSD1306_BlitRect(u8 c1, u8 p1, u8 c2, u8 p2, u8 *data) {
	u8 c,p;
	u8 *ptr;
	u16 ofs=0;

	for (p=p1; p <= p2; p++) {
		ptr = &ssd1306_screenbuffer[p*SSD1306_SIZE_X+c1];
		for (c=c1; c <= c2; c++) {
			*ptr = data[ofs++];
		}
	}
}

/**
 * Draw a string on the screen
 * @param x x coordinate of upper left corner
 * @param y y coordinate of upper left corner
 * @param str string to print
 * @param font font to use
 * @param set 0: clear, 1: set
 */
void SSD1306_DrawStr(u8 x, u8 y, char* str, font_t *font, u8 set) {
	u8  x_idx, y_idx, c_idx, xofs, y_act;
	u16 y_ofs;
	u8 strl = strlen(str);
	u8 fwidth = font->width;
	u8 fheight = font->height;

	// go through text line by line
	for (y_idx=0; y_idx<fheight; y_idx++) {
		y_act = y+y_idx;
		y_ofs = (y_act/8)*SSD1306_SIZE_X+x;
		u8 mask = 1<<(y_act&7);

		xofs=0;
		// go through text character by character
		for (c_idx=0; c_idx<strl; c_idx++) {
			u8 c = str[c_idx]; // get character
			u8 *data_ptr = (u8*)&font->data[((c - 32) * fheight + y_idx) * fwidth/8]; // get data
			u8 data = *data_ptr;
			u8 inc_data_ptr = 0;
			// go through character column by column
			for (x_idx=0; x_idx<fwidth; x_idx++, inc_data_ptr = ((x_idx & 7) == 0)) {
				if (inc_data_ptr)
					data = *(++data_ptr);
				if ((data & 0x80)!=0) {
					u8 *ptr = &ssd1306_screenbuffer[y_ofs+xofs];
					if (set)
						*ptr |= mask;
					else
						*ptr &= (u8)~mask;
				}
				xofs++;
				data <<= 1;
			}
		}
	}
}

/**
 * Update area of SSD1306 GDDRAM
 * @param c1 x/column coordinate of upper left corner
 * @param p1 page of upper left corner
 * @param c2 x coordinate of lower right corner
 * @param p1 page coordinate of lower right corner
 * @param set 0: clear, 1: set
 */
void SSD1306_UpdateRect(u8 c1, u8 p1, u8 c2, u8 p2) {
	u8 p;
	u8 *ptr = &ssd1306_screenbuffer[p1*SSD1306_SIZE_X+c1];
	u8 len = c2-c1+1;

	SSD1306_SetArea(c1, p1, c2, p2);
	for (p=p1; p<=p2; p++) {
		SSD1306_WriteData(ptr, len);
		ptr += SSD1306_SIZE_X;
	}
}

void SSD1306_UpdateScreen(void) {
	SSD1306_SetArea(0,0,SSD1306_SIZE_X-1,7);
	SSD1306_WriteData(ssd1306_screenbuffer, sizeof(ssd1306_screenbuffer));
}

/**
 * Initialize the display.
 */
void SSD1306_Init(void) {
  SSD1306_WriteCommand(SSD1306_DISPLAY_ON(0));     // switch off
  SSD1306_WriteCommand(SSD1306_DISPLAY_CLOCK_DIV);
  SSD1306_WriteCommand(0x80);                      // lower nibble: divider = 1. higher nibble: oscillator frequency = 8 (370kHz?)
  SSD1306_WriteCommand(SSD1306_SET_MULTIPLEX);
#if SSD1306_SIZE_Y==64
    SSD1306_WriteCommand(0x3F);
#else
    SSD1306_WriteCommand(0x1F);
#endif
  SSD1306_WriteCommand(SSD1306_SET_DISPLAY_OFS);
  SSD1306_WriteCommand(0x0);
  SSD1306_WriteCommand(SSD1306_SET_START_LINE(0));
  SSD1306_WriteCommand(SSD1306_CHARGEPUMP);
  SSD1306_WriteCommand(0x14);                      // Enable Charge Pump
  SSD1306_WriteCommand(SSD1306_MEMORY_ADR_MODE);
  SSD1306_WriteCommand(0x00);                      // horizontal mode
  SSD1306_WriteCommand(SSD1306_SEGMENT_REMAP(1));  // column address 127 is mapped to SEG0
  SSD1306_WriteCommand(SSD1306_COM_SCAN_DIR(8));   // COM[N-1] to COM0
  SSD1306_WriteCommand(SSD1306_SETCOMPINS);
#if SSD1306_SIZE_Y==64
    SSD1306_WriteCommand(0x12);                    // Alternative COM pin configuration
#else
    SSD1306_WriteCommand(0x02);                    // Sequential COM pin configuration
#endif
  SSD1306_WriteCommand(SSD1306_SET_CONTRAST);
  SSD1306_WriteCommand(SSD1306_CONTRAST_DEFAULT);
  SSD1306_WriteCommand(SSD1306_SET_PRECHARGE);
  SSD1306_WriteCommand(0xF1);                      // lower nibble: phase1=1 DCLK, higher nibble: phase2=15 DCLK
  SSD1306_WriteCommand(SSD1306_VCOMH_DESELECT_LVL);
  SSD1306_WriteCommand(0x40);                      // > 0.83xVCC (no value >0x30 given in data sheet)
  SSD1306_WriteCommand(SSD1306_ENTIRE_DISP_ON(0)); // all pixels on after reset -> use GDDRAM instead
  SSD1306_WriteCommand(SSD1306_INVERT_DISPLAY(0)); // don't invert
  SSD1306_WriteCommand(SSD1306_DISPLAY_ON(1));     // switch on
}
