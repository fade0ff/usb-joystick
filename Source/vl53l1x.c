/** VL53L1X Time of Flight Sensor Library
	Communication through I2C mode
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2019 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "i2c.h"
#include "systime.h"
#include "vl53l1x.h"

u8  vl53l1x_data_read[4];
u8  vl53l1x_data_write[4];

u16 v53l1x_fast_osc_frequency;
u16 vl53l1x_osc_calibrate_val;

//static const u32 TimingGuard = 4528;
static const u16 TargetRate = 0x0A00;

/** Start 8bit write transfer
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 VL53L1X_Write8(u16 reg, u8 value) {
	vl53l1x_data_write[0] = value;
	while(I2C_GetChannelPendingCount(VL53L1X_I2C_CH)>0);
	return I2C_MasterTransfer(VL53L1X_I2C_CH, I2C_TYPE_MST_WRITE, reg, &vl53l1x_data_write[0], 1, NULL);
}


/** Start 16bit write transfer
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 VL53L1X_Write16(u16 reg, u16 value) {
	vl53l1x_data_write[0] = (u8)(value >> 8);
	vl53l1x_data_write[1] = (u8)value;
	while(I2C_GetChannelPendingCount(VL53L1X_I2C_CH)>0);
	return I2C_MasterTransfer(VL53L1X_I2C_CH, I2C_TYPE_MST_WRITE, reg, &vl53l1x_data_write[0], 2, NULL);
}

/** Start 32bit write transfer
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 VL53L1X_Write32(u16 reg, u16 value) {
	vl53l1x_data_write[0] = (u8)(value >> 24);
	vl53l1x_data_write[1] = (u8)(value >> 16);
	vl53l1x_data_write[2] = (u8)(value >> 8);
	vl53l1x_data_write[3] = (u8)value;
	while(I2C_GetChannelPendingCount(VL53L1X_I2C_CH)>0);
	return I2C_MasterTransfer(VL53L1X_I2C_CH, I2C_TYPE_MST_WRITE, reg, &vl53l1x_data_write[0], 4, NULL);
}

/** Read 8bit
	@return value read
 */
u8 VL53L1X_Read8(u16 reg) {
	I2C_MasterTransfer(VL53L1X_I2C_CH, I2C_TYPE_MST_READ, reg, &vl53l1x_data_read[0], 1, NULL);
	while(I2C_GetChannelPendingCount(VL53L1X_I2C_CH)>0);
	return vl53l1x_data_read[0];
}

/** Read 16bit
	@return value read
 */
u16 VL53L1X_Read16(u16 reg) {
	I2C_MasterTransfer(VL53L1X_I2C_CH, I2C_TYPE_MST_READ, reg, &vl53l1x_data_read[0], 2, NULL);
	while(I2C_GetChannelPendingCount(VL53L1X_I2C_CH)>0);
	return ((u16)vl53l1x_data_read[0]<<8) | (u16)vl53l1x_data_read[1];
}

/** Read 32bit
	@return value read
 */
u32 VL53L1X_Read32(u16 reg) {
	I2C_MasterTransfer(VL53L1X_I2C_CH, I2C_TYPE_MST_READ, reg, &vl53l1x_data_read[0], 4, NULL);
	while(I2C_GetChannelPendingCount(VL53L1X_I2C_CH)>0);
	return ((u32)vl53l1x_data_read[0]<<24) | ((u32)vl53l1x_data_read[1]<<16) | ((u32)vl53l1x_data_read[2]<<8) | (u32)vl53l1x_data_read[3];
}

/** Read n bytes
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 VL53L1X_StartRead(u16 reg, u8 *buffer, u8 len) {
	return I2C_MasterTransfer(VL53L1X_I2C_CH, I2C_TYPE_MST_READ, reg, buffer, len, NULL);
}


void VL53L1X_SetDistanceMode(u8 mode) {

}

void VL53L1X__SetMeasurementTimingBudget(u16 budget) {

}

void VL53L1X_Init(void) {
	VL53L1X_Write8(VL53L1X_SOFT_RESET, 0x00);
	SYSTIME_WaitUs(100);
	VL53L1X_Write8(VL53L1X_SOFT_RESET, 0x01);
	SYSTIME_WaitUs(1000);
	while ((VL53L1X_Read8(VL53L1X_FIRMWARE_SYSTEM_STATUS) & 0x01) == 0);

	v53l1x_fast_osc_frequency = VL53L1X_Read16(VL53L1X_OSC_MEASURED_FAST_OSC_FREQUENCY_HI);
	vl53l1x_osc_calibrate_val = VL53L1X_Read16(VL53L1X_RESULT_OSC_CALIBRATE_VAL_HI);

	// static config
	VL53L1X_Write16(VL53L1X_DSS_CONFIG_TARGET_TOTAL_RATE_MCPS_HI, TargetRate);
	VL53L1X_Write8(VL53L1X_GPIO_TIO_HV_STATUS, 2);
	VL53L1X_Write8(VL53L1X_SIGMA_ESTIMATOR_EFFECTIVE_PULSE_WIDTH_NS, 8);
	VL53L1X_Write8(VL53L1X_SIGMA_ESTIMATOR_EFFECTIVE_AMBIENT_WIDTH_NS, 16);
	VL53L1X_Write8(VL53L1X_ALGO_CROSSTALK_COMPENSATION_VALID_HEIGHT_MM, 1);
	VL53L1X_Write8(VL53L1X_ALGO_RANGE_IGNORE_VALID_HEIGHT_MM, 0xFF);
	VL53L1X_Write8(VL53L1X_ALGO_RANGE_MIN_CLIP, 0);
	VL53L1X_Write8(VL53L1X_ALGO_CONSISTENCY_CHECK_TOLERANCE, 2);

	// general config
	VL53L1X_Write16(VL53L1X_SYSTEM_THRESH_RATE_HIGH_HI, 0x0000);
	VL53L1X_Write16(VL53L1X_SYSTEM_THRESH_RATE_LOW_HI, 0x0000);
	VL53L1X_Write8(VL53L1X_DSS_CONFIG_APERTURE_ATTENUATION, 0x38);

	// timing config
	VL53L1X_Write16(VL53L1X_RANGE_CONFIG_SIGMA_THRESH_HI, 360);
	VL53L1X_Write16(VL53L1X_RANGE_CONFIG_MIN_COUNT_RATE_RTN_LIMIT_MCPS_HI, 192);

	// dynamic config
	VL53L1X_Write8(VL53L1X_SYSTEM_GROUPED_PARAMETER_HOLD_0, 0x01);
	VL53L1X_Write8(VL53L1X_SYSTEM_GROUPED_PARAMETER_HOLD_1, 0x01);
	VL53L1X_Write8(VL53L1X_SD_CONFIG_QUANTIFIER, 2);

	VL53L1X_Write8(VL53L1X_SYSTEM_GROUPED_PARAMETER_HOLD, 0x00);
	VL53L1X_Write8(VL53L1X_SYSTEM_SEED_CONFIG, 1);

	// from VL53L1_config_low_power_auto_mode
	VL53L1X_Write8(VL53L1X_SYSTEM_SEQUENCE_CONFIG, 0x8B); // VHV, PHASECAL, DSS1, RANGE
	VL53L1X_Write16(VL53L1X_GPH_DSS_CONFIG_MANUAL_EFFECTIVE_SPADS_SELECT_HI, 200 << 8);
	VL53L1X_Write8(VL53L1X_DSS_CONFIG_ROI_MODE_CONTROL, 2); // REQUESTED_EFFFECTIVE_SPADS

	// default to long range, 50ms timing budget
	VL53L1X_SetDistanceMode(VL53L1X_DIST_MODE_LONG);
	VL53L1X__SetMeasurementTimingBudget(50000);

	// the API triggers this change in VL53L1_init_and_start_range() once a
	// measurement is started; assumes MM1 and MM2 are disabled
	VL53L1X_Write16(VL53L1X_ALGO_PART_TO_PART_RANGE_OFFSET_MM_HI, VL53L1X_Read16(VL53L1X_MM_CONFIG_OUTER_OFFSET_MM_HI) * 4);
}
